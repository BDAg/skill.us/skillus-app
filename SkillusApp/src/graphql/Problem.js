import gql from 'graphql-tag';

export const PROBLEM_LIST = gql`
    query {
        problems {
            id
            name
            description
            status
            color
            icon
            skill {
                name
                color
            }
            creator {
                id
                name
            }
        }
    }
`;
export const CREATE_PROBLEM = gql`
    mutation createProblem($name: String!, $description: String!, $createdBy: Int!, $skill: [Int]) {
        createProblem(name: $name, description: $description, createdBy: $createdBy, skill: $skill)
    }
`;

export const CLOSE_PROBLEM = gql`
    mutation closeProblem($problem_id: ID!, $note: Float!, $comment: String!) {
        closeProblem(problem_id: $problem_id, note: $note, comment: $comment) {
            id
            name
            status
        }
    }
`;

export const ADD_PROBLEM_HELPER = gql`
    mutation addProblemHelper($user_id: ID!, $problem_id: ID!) {
        addProblemHelper(user_id: $user_id, problem_id: $problem_id) {
            status
        }
    }
`;
