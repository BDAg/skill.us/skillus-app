import gql from 'graphql-tag';

export const ProblemByUser = gql`
    query {
        problemsByUser(user_id: 4) {
            id
            name
            color
            icon
            skill {
                name
                color
            }
        }
    }
`;
