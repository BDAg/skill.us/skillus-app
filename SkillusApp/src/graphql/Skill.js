import gql from 'graphql-tag';

//variavel que traz a query com as informaçoes do banco
export const listSkills = gql`
    query {
        skills {
            id
            name
        }
    }
`;
