import gql from 'graphql-tag';

//variavel que traz a query com as informaçoes do banco
export const USER_LIST = gql`
    query {
        users {
            id
            name
            media
            description
            contact {
                id
                name
                icon
            }
            skill {
                id
                name
                rating
            }
        }
    }
`;

export const GET_MY_USER = gql`
    query {
        eu {
            id
            name
            media
            description
        }
    }
`;

export const REGISTER_USER = gql`
    mutation register(
        $name: String!
        $email: String!
        $password: String!
        $telephone: String
        $description: String
        $skill: [Int]
        $contact: [String]
    ) {
        register(
            name: $name
            email: $email
            password: $password
            telephone: $telephone
            description: $description
            skill: $skill
            contact: $contact
        )
    }
`;
