import gql from 'graphql-tag';

//variavel que traz a query com as informaçoes do banco
export const listProblems = gql`
    query {
        problems {
            id
            name
            description
            status
            color
            icon
            skill {
                name
                color
            }
            creator {
                id
                name
            }
        }
    }
`;
