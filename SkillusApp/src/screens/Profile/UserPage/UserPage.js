import React, { Component, useState } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import styles from './Styles';

import ProfileAbout from '../../../components/Profile/ProfileAbout/ProfileAbout';
import ProfileSkills from '../../../components/Profile/ProfileSkills/ProfileSkills';
import ProfileContact from '../../../components/Profile/ProfileContact/ProfileContact';
import { useQuery } from 'graphql-tag';
import { GET_MY_USER } from '../../../graphql/User';

const UserPage = ({ route }) => {
    const [usuario, setUsuario] = useState('');
    if (route.params.user) {
        setUsuario(route.params.user);
    }
    const { loading, error, data } = useQuery(GET_MY_USER);
    if (loading) {
        return null;
    }

    return (
        <ScrollView>
            <View style={styles.page}>
                <ProfileAbout user={usuario ? usuario : data.eu} />
                <ProfileSkills user={usuario ? usuario : data.eu} />
                <ProfileContact user={usuario ? usuario : data.eu} />
            </View>
        </ScrollView>
    );
};

export default UserPage;

// Tab navigate
