import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    page: {
        paddingTop: 70,
        // backgroundColor: "#f8f8f8",
    },

    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },

    button_container: {
        flex: 1,
    },

    button_selected: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#F77F5C',
        color: '#F77F5C',
    },

    button: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
    },
});

export default styles;
