import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    body: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },

    container: {
        width: 332,
        height: 522,
        alignItems: 'center',
        backgroundColor: '#ffffffD9',
        borderWidth: 1,
        borderColor: '#ffffffB3',
        borderRadius: 5,
    },

    imgContainer: {
        width: 145,
        height: 145,
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 29,
    },

    img: {
        width: 145,
        height: 145,
    },

    searchSection: {
        width: 262,
        height: 55,
        borderWidth: 1,
        borderColor: '#F77F5C',
        marginTop: 6,
        marginBottom: 6,
        shadowColor: '#000',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 1,
    },

    searchIcon: {
        padding: 10,
    },

    input: {
        width: 215,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        borderRadius: 5,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },

    button: {
        marginTop: 35,
        alignItems: 'center',
        textAlignVertical: 'center',
        borderRadius: 5,
        backgroundColor: '#F77F5C',
        height: 55,
        width: 262,
    },

    buttonText: {
        textAlign: 'center',
        textAlignVertical: 'center',
        height: 55,
        width: 262,
        fontWeight: '300',
        color: '#ffffff',
        fontSize: 24,
    },

    noticeContainer: {
        paddingTop: 20,
        alignItems: 'center',
    },

    noticeText(indentify = 'normal') {
        return {
            color: '#882D52',
            fontSize: 15,
            fontWeight: indentify,
        };
    },
});
