import React, { useContext, useState } from 'react';
import { View, Text, TextInput, TouchableHighlight, Image } from 'react-native';
import { useMutation } from '@apollo/react-hooks';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { LOGIN } from '../../../graphql/Login';
import LoginContext from '../../../contexts/LoginContext';
import Styles from './Styles';

const LoginPage = (props) => {
    const [login, responseApollo] = useMutation(LOGIN);
    const context = useContext(LoginContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const Login = async () => {
        const response = await login({
            variables: {
                email: 'mateusteodorovieirabaptista@gmail.com',
                password: 'MTvieira12',
            },
        });
        context.updateToken(response.data.login.accessToken);
    };

    return (
        <LinearGradient colors={['#F77F5C', '#882D52']}>
            <View style={Styles.body}>
                <View style={Styles.container}>
                    <View style={Styles.imgContainer}>
                        <Image style={Styles.img} source={require('../../../assets/Logo.png')} />
                    </View>
                    <View>
                        <View style={Styles.searchSection}>
                            <Icon name={'user'} style={Styles.searchIcon} backgroundColor="transparent" color="#282828CC" size={30} />
                            <TextInput
                                style={Styles.input}
                                placeholder="Email"
                                keyboardType="email-address"
                                placeholderTextColor="#28282880"
                                underlineColorAndroid="transparent"
                                onChangeText={(value) => setEmail(value)}
                            />
                        </View>

                        <View style={Styles.searchSection}>
                            <Icon name={'lock'} style={Styles.searchIcon} backgroundColor="transparent" color="#282828CC" size={30} />
                            <TextInput
                                style={Styles.input}
                                placeholder="Senha"
                                keyboardType="default"
                                placeholderTextColor="#28282880"
                                underlineColorAndroid="transparent"
                                onChangeText={(value) => setPassword(value)}
                            />
                        </View>
                        <TouchableHighlight style={Styles.button} onPress={Login}>
                            <Text style={Styles.buttonText}> Entrar </Text>
                        </TouchableHighlight>
                    </View>
                    <View style={Styles.noticeContainer}>
                        <Text style={Styles.noticeText()}>Não possui conta?</Text>
                        <Text style={Styles.noticeText('bold')} onPress={() => props.navigation.navigate('RegisterUser')}>
                            Cadatre-se já
                        </Text>
                    </View>
                </View>
            </View>
        </LinearGradient>
    );
};

export default LoginPage;
