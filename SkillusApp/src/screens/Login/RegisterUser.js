import React, { useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Text, View, ScrollView, TextInput, StyleSheet, Alert, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './Styles';
import { useQuery, useMutation } from '@apollo/react-hooks';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { listSkills } from '../../graphql/Skill';
import { Form, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text';
import { REGISTER_USER } from '../../graphql/User';

const RegisterUser = ({ navigation }) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [description, setDescription] = useState(false);
    const [whatsapp, setWhatsapp] = useState('');
    const [linkedin, setLinkedin] = useState('');
    const [git, setGit] = useState('');
    const { loading, error, data } = useQuery(listSkills);
    const [selectedSkills, SetSelectedSkills] = useState([]);
    const [register, responseMutation] = useMutation(REGISTER_USER);

    const [contact, setContact] = useState([]);

    const onSelectionsChange = (skill) => {
        SetSelectedSkills(skill);
    };

    if (loading) {
        return (
            <View style={styles.flexCenter}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }
    const cadastrar = async () => {
        if (name && email && password && description && whatsapp) {
            const response = await register({
                variables: {
                    name,
                    email,
                    password,
                    telephone: whatsapp,
                    description,
                    photo: 'avatar',
                    skill: selectedSkills.map((i) => Number(i)),
                    contact: [linkedin, git, whatsapp],
                },
            });
            alert('Cadastro feito com sucesso!');
            navigation.navigate('Login');
        } else {
            alert('Por favor verifique os campos!');
        }
    };

    return (
        <ScrollView>
            <LinearGradient colors={['#ffa800', '#bd3d00']}>
                <View style={styles.box}>
                    <TextInput
                        style={styles.email}
                        placeholder=" Email"
                        onChangeText={(value) => setEmail(value)}
                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                        required
                    />
                    <TextInput style={styles.user} placeholder=" Nome de Usuário" onChangeText={(value) => setName(value)} />
                    <TextInput style={styles.user} placeholder=" Senha" secureTextEntry={true} onChangeText={(value) => setPassword(value)} />
                    <TextInput style={styles.user} placeholder=" Confirmar Senha" secureTextEntry={true} onChangeText={(value) => setPassword(value)} />
                    <Text style={styles.dadosPessoais}> Dados Pessoais </Text>
                    <Text style={styles.underline}> ________________ </Text>
                    <TextInput
                        style={styles.descriptionBox}
                        multiline={true}
                        placeholder=" Descreva seu Perfil"
                        onChangeText={(value) => setDescription(value)}
                    />
                    <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center' }}>
                        <Icon name="whatsapp" color="black" style={styles.iconwhats} />
                        <TextInput
                            style={styles.whats}
                            keyboardType={'numeric'}
                            type={'cel-phone'}
                            placeholder=" (xx)xxxxx-xxxx"
                            onChangeText={(value) => setWhatsapp(value)}
                            options={{
                                maskType: 'BR',
                                withDDD: true,
                                dddMask: '(99) ',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center' }}>
                        <Icon name="linkedin" color="black" style={styles.iconlinkedin} />
                        <TextInput style={styles.linkedin} placeholder=" LinkedIn" onChangeText={(value) => setLinkedin(value)} />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center' }}>
                        <Icon name="gitlab" color="black" style={styles.icongit} />
                        <TextInput style={styles.linkedin} placeholder=" Gitlab" onChangeText={(value) => setGit(value)} />
                    </View>
                    <View style={styles.boxInput}>
                        <Text style={styles.titleInput}>Tags</Text>
                        <SectionedMultiSelect
                            items={data.skills}
                            uniqueKey="id"
                            selectText="Selecione as habilidades..."
                            searchPlaceholderText="Procure pela habilidade"
                            onSelectedItemsChange={onSelectionsChange}
                            selectedItems={selectedSkills}
                            colors={{ primary: '#F77F5C' }}
                            styles={{ itemText: { padding: '3%' }, confirmText: { padding: '3%' } }}
                        />
                    </View>

                    <View>
                        <TouchableOpacity style={styles.botaoSalvar} onPress={cadastrar}>
                            <Text style={styles.TextBotaoSalvar}> Cadastrar </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        </ScrollView>
    );
};

export default RegisterUser;
