import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { USER_LIST } from '../../graphql/User';
import LinearGradient from 'react-native-linear-gradient';
import { Searchbar, RadioButton } from 'react-native-paper';
import ItemRankingBox from '../../components/Ranking/ItemRankingBox';
import { ScrollView, View, StyleSheet, Text, ActivityIndicator } from 'react-native';

const UserList = ({ navigation }) => {
    const { loading, error, data } = useQuery(USER_LIST);
    const [search, setSearch] = useState('');
    const [checked, setChecked] = useState('Nome');
    const searchString = search.trim().toLowerCase();

    if (loading) {
        return (
            <View style={styles.flexCenter}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }

    RegExp.quote = function(str) {
        return str.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
    };

    const matchingRegex = new RegExp(RegExp.quote(searchString));

    const filteredArrayByName = data.users.filter(user => {
        return user.name.toLowerCase().match(matchingRegex);
    });

    const filteredArrayBySkill = data.users.filter(user => {
        if (user.skill.lenght > 0) return user.skill[0].name.toLowerCase().match(matchingRegex);
        else return null;
    });

    return (
        <ScrollView>
            <View style={styles.container}>
                <Searchbar
                    // icon={}
                    placeholder="Pesquisar"
                    onChangeText={query => setSearch(query)}
                    value={search}
                />
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <RadioButton value="Nome" status={checked === 'Nome' ? 'checked' : 'unchecked'} onPress={() => setChecked('Nome')} />
                    <Text style={{ marginRight: 30 }}>Nome</Text>
                    <RadioButton value="Habilidade" status={checked === 'Habilidade' ? 'checked' : 'unchecked'} onPress={() => setChecked('Habilidade')} />
                    <Text>Habilidade</Text>
                </View>
                <LinearGradient colors={['#fbb7a3', '#fff']}>
                    <ItemRankingBox users={checked === 'Nome' ? filteredArrayByName : filteredArrayBySkill} navigation={navigation} />
                </LinearGradient>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    flexCenter: {
        flex: 1,
        justifyContent: 'center',
    },
});

export default UserList;
