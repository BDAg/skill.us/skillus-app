import React from 'react';
import { Text, View, ScrollView, ActivityIndicator, TouchableOpacity, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { PROBLEM_LIST } from '../../graphql/Problem';
import { useQuery } from '@apollo/react-hooks';
import ProblemBox from '../../components/Problem/ProblemBox';

const ListProblem = (props) => {
    const { data, loading, error } = useQuery(PROBLEM_LIST);

    if (loading) {
        return (
            <View>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }

    return (
        <View>
            <LinearGradient colors={['#fbb7a3', '#fff']} style={{ height: '100%' }}>
                <ScrollView>
                    {data.problems.map((problem) => (
                        <ProblemBox key={problem.id} problem={problem} navigation={props.navigation} />
                    ))}
                </ScrollView>
                <TouchableOpacity style={styles.floatButton} onPress={props.navigation.navigate('ProblemAdd')}>
                    <Text>Novo</Text>
                </TouchableOpacity>
            </LinearGradient>
        </View>
    );
};

export default ListProblem;

const styles = StyleSheet.create({
    floatButton: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        backgroundColor: '#F77F5C',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 70,
        borderRadius: 100,
    },
});
