import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TouchableHighlight, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ProblemAbout from '../../components/Problem/ProblemAbout/ProblemAbout';
import ProblemSkill from '../../components/Problem/ProblemSkill/ProblemSkill';
import ProblemDescription from '../../components/Problem/ProblemDescription/ProblemDescription';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useMutation } from '@apollo/react-hooks';
import { ADD_PROBLEM_HELPER } from '../../graphql/Problem';

const Tab = createMaterialTopTabNavigator();

const Problem = ({ route }) => {
    const { problem } = route.params;

    const [addProblemHelper, responseMutation] = useMutation(ADD_PROBLEM_HELPER);

    const help = async () => {
        try {
            const response = await addProblemHelper({
                variables: {
                    problem_id: 57,
                    user_id: 8,
                },
            });
            alert('Parabéns! você agora esta ajudando este problema!');
        } catch (e) {
            alert('Erro ao ajudar problema!');
            console.log(e.networkError.result.errors);
        }
    };

    return (
        <View style={style.container}>
            <ScrollView>
                <View>
                    <Text> {problem.creator.name}</Text>
                </View>
                <View style={style.content}>
                    <ProblemSkill problem={problem} />
                    <Text style={style.description}>{problem.description}</Text>
                </View>
                <TouchableOpacity style={style.button} onPress={help}>
                    <Text style={style.buttonText}>Ajudar</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
};

export default Problem;

const style = StyleSheet.create({
    button: {
        flex: 1,
        marginTop: 15,
        alignItems: 'center',
        backgroundColor: '#F77F5C',
        padding: '3%',
        borderRadius: 7,
    },
    buttonText: {
        color: '#FFF',
        fontSize: 20,
    },
    container: {
        height: '100%',
        backgroundColor: '#FFF',
        padding: '5%',
    },
    scrollPage: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    content: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 20,
    },
    description: {
        backgroundColor: '#F1F1F1',
        height: '50%',
        marginTop: 15,
        marginBottom: 15,
    },
});
