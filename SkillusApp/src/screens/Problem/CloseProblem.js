import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Rating } from 'react-native-elements';
import { useMutation } from '@apollo/react-hooks';
import { CLOSE_PROBLEM } from '../../graphql/Problem';

const CloseProblem = (props) => {
    const [note, setNote] = useState(undefined);
    const [comment, setComment] = useState('');
    const [closeProblem, responseMutation] = useMutation(CLOSE_PROBLEM);

    const ratingCompleted = (rating) => {
        setNote(rating);
    };

    const close = async () => {
        if (note && comment) {
            try {
                const response = await closeProblem({
                    variables: {
                        problem_id: 57,
                        note,
                        comment,
                    },
                });
                props.navigation.navigate('ProblemList');
            } catch (e) {
                alert('Erro ao fechar problema!');
                console.log(e.networkError.result.errors);
            }
        } else {
            alert('Preencha todos os campos!');
        }
    };

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.boxInput}>
                    <Text style={styles.titleClose}>Carlos Rodrigues está te ajudando neste problema</Text>
                </View>
                <View style={styles.boxInput}>
                    <Text style={styles.titleInput}>Avalie a pessoa que te ajudou!</Text>
                    <Rating
                        type="custom"
                        ratingColor="#f1c40f"
                        ratingBackgroundColor="#fff"
                        ratingCount={5}
                        startingValue={0}
                        imageSize={60}
                        onFinishRating={ratingCompleted}
                        style={{ paddingVertical: 10 }}
                    />
                    <TextInput
                        placeholder="Escreva um comentário..."
                        style={styles.input}
                        multiline={true}
                        numberOfLines={10}
                        value={comment}
                        onChangeText={(value) => setComment(value)}
                    />
                </View>
                <TouchableOpacity style={styles.button} onPress={close}>
                    <Text style={styles.buttonText}>Enviar</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
};

export default CloseProblem;

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#FFF',
        padding: '5%',
    },
    titleInput: {
        fontSize: 20,
        textAlignVertical: 'top',
        textAlign: 'center',
    },
    titleClose: {
        fontSize: 22,
        textAlignVertical: 'top',
        textAlign: 'center',
        color: '#F77F5C',
    },
    boxInput: {
        marginBottom: 15,
    },
    button: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F77F5C',
        padding: '3%',
        borderRadius: 7,
    },
    buttonText: {
        color: '#FFF',
        fontSize: 20,
    },
    input: {
        backgroundColor: '#F1F1F1',
        paddingLeft: '3%',
        borderRadius: 7,
        fontSize: 18,
        textAlignVertical: 'top',
    },
});
