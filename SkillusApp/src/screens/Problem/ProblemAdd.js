import React, { useState } from 'react';
import { Text, View, ScrollView, TextInput, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { useQuery, useMutation } from '@apollo/react-hooks';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { listSkills } from '../../graphql/Skill';
import { CREATE_PROBLEM } from '../../graphql/Problem';

const ProblemAdd = () => {
    const { loading, error, data } = useQuery(listSkills);
    const [createProblem, responseMutation] = useMutation(CREATE_PROBLEM);
    const [selectedSkills, setSelectedSkills] = useState([]);
    const [name, setName] = useState(false);
    const [description, setDescription] = useState(false);

    if (loading) {
        return (
            <View style={styles.flexCenter}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }

    const onSelectionsChange = async (skill) => {
        setSelectedSkills(skill);
    };

    const addProblem = async () => {
        if (name && description) {
            const response = await createProblem({
                variables: {
                    name,
                    description,
                    createdBy: 5,
                    skill: selectedSkills.map((i) => Number(i)),
                },
            });
            if (response.data.createProblem === true) {
                alert('Problema adicionado com sucesso!');
            }
        } else {
            alert('Preencha todos os campos!');
        }
    };

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.boxInput}>
                    <Text style={styles.titleInput}>Assunto</Text>
                    <TextInput style={styles.input} value={name} onChangeText={(value) => setName(value)} />
                </View>
                <View style={styles.boxInput}>
                    <Text style={styles.titleInput}>Descreva seu problema</Text>
                    <TextInput style={styles.input} multiline={true} numberOfLines={10} value={description} onChangeText={(value) => setDescription(value)} />
                </View>
                <View style={styles.boxInput}>
                    <Text style={styles.titleInput}>Tags</Text>
                    <SectionedMultiSelect
                        items={data.skills}
                        uniqueKey="id"
                        selectText="Selecione as habilidades..."
                        searchPlaceholderText="Procure pela habilidade"
                        onSelectedItemsChange={onSelectionsChange}
                        selectedItems={selectedSkills}
                        colors={{ primary: '#F77F5C' }}
                        styles={{ itemText: { padding: '3%' }, confirmText: { padding: '3%' } }}
                    />
                </View>
                <TouchableOpacity style={styles.button} onPress={addProblem}>
                    <Text style={styles.buttonText}>Postar</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
};

export default ProblemAdd;

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#FFF',
        padding: '5%',
    },
    boxInput: {
        marginBottom: 15,
    },
    titleInput: {
        fontSize: 20,
        textAlignVertical: 'top',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F77F5C',
        padding: '3%',
        borderRadius: 7,
    },
    buttonText: {
        color: '#FFF',
        fontSize: 20,
    },
    input: {
        backgroundColor: '#F1F1F1',
        paddingLeft: '3%',
        borderRadius: 7,
        fontSize: 18,
        textAlignVertical: 'top',
    },
    flexCenter: {
        flex: 1,
        justifyContent: 'center',
    },
});
