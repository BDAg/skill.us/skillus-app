/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, ScrollView, ActivityIndicator } from 'react-native';
import { useQuery } from '@apollo/react-hooks';
import { ProblemByUser } from '../../graphql/ProblemByUser';
import LinearGradient from 'react-native-linear-gradient';

import MyProblemBox from '../../components/Problem/MyProblemBox';

const ListMyProblem = () => {
    const { data, loading } = useQuery(ProblemByUser);
    if (loading) {
        return (
            <View>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }

    return (
        <View>
            <LinearGradient colors={['#fbb7a3', '#fff']} style={{ height: '100%' }}>
                <ScrollView>
                    {data.problemsByUser.map((problem) => (
                        <MyProblemBox problem={problem} />
                    ))}
                </ScrollView>
            </LinearGradient>
        </View>
    );
};

export default ListMyProblem;
