import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProblemList from '../screens/Problem/ListAll';
import ProblemAdd from '../screens/Problem/ProblemAdd';
import CloseProblem from '../screens/Problem/CloseProblem';
import ListMyProblem from '../screens/Problem/ListMyProblem';
import Problem from '../screens/Problem/Problem';

const Stack = createStackNavigator();

const ProblemStack = () => {
    return (
        <Stack.Navigator initialRouteName="ProblemList" headerMode="screen">
            <Stack.Screen name="ProblemList" component={ProblemList} />
            <Stack.Screen name="ProblemAdd" component={ProblemAdd} />
            <Stack.Screen
                name="CloseProblem"
                component={CloseProblem}
                options={{
                    title: 'Fechar Problema #010',
                }}
            />
            <Stack.Screen name="ListMyProblem" component={ListMyProblem} />
            <Stack.Screen
                name="Problem"
                component={Problem}
                options={({ route }) => ({
                    title: `##${route.params.problem.id} ${route.params.problem.name}`,
                    headerTitleAlign: 'center',
                })}
            />
        </Stack.Navigator>
    );
};

export default ProblemStack;
