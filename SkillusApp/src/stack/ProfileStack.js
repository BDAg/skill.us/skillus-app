import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import UserPage from '../screens/Profile/UserPage/UserPage';
const Stack = createStackNavigator();

const ProfileStack = () => {
    return (
        <Stack.Navigator
            initialRouteName="UserPage"
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="UserPage" component={UserPage} />
        </Stack.Navigator>
    );
};

export default ProfileStack;
