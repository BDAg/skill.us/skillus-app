import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RegisterUser from '../screens/Login/RegisterUser';
import Login from '../screens/Login/LoginPage/LoginPage';

const Stack = createStackNavigator();

const LoginStack = () => {
    return (
        <Stack.Navigator
            initialRouteName="Login" //importar login
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="RegisterUser" component={RegisterUser} />
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
    );
};

export default LoginStack;
