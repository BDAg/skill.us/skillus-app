import 'react-native-gesture-handler';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import UserProblems from '../screens/Ranking/UserProblems';
import UserList from '../screens/Ranking/UserList';
import UserPage from '../screens/Profile/UserPage/UserPage';

const Stack = createStackNavigator();

const RankingStack = () => {
    return (
        <Stack.Navigator
            initialRouteName="UserList"
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="UserList" component={UserList} />
            <Stack.Screen name="UserPage" component={UserPage} />
            <Stack.Screen name="UserProblems" component={UserProblems} />
        </Stack.Navigator>
    );
};

export default RankingStack;
