import React, { useState } from 'react';
import { View, TextInput } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

const SearchBar = () => {
    return (
        <View style={styles.container}>
            <FontAwesomeIcon icon={faSearch} style={styles.icon} />
            <TextInput
                placeholder="Procurar por alguém"
                placeholderTextColor={'#7519c1'}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                style={styles.input}
            />
        </View>
    );
};

export default SearchBar;
