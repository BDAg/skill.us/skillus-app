import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  input: {
    marginTop: 10,
    paddingLeft: 30,
    width: '90%',
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 25,
    borderWidth: 1.2,
    borderColor: '#7159c1',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  icon: {
    color: '#7159c1',
    margin: 5,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});

export default styles;
