import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import RankingStack from '../../stack/RankingStack';
import ProblemStack from '../../stack/ProblemStack';
import ProfileStack from '../../stack/ProfileStack';
import ListMyProblem from '../../screens/Problem/ListMyProblem';

const Tab = createBottomTabNavigator();

const BottomNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: '#FF8824',
            }}>
            <Tab.Screen name="Home" component={RankingStack} />
            <Tab.Screen name="Problem" component={ProblemStack} />
            <Tab.Screen name="Profile" component={ProfileStack} />
            <Tab.Screen name="ListMyProblem" component={ListMyProblem} />
        </Tab.Navigator>
    );
};

export default BottomNavigator;
