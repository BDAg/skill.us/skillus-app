import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    card: {
        flexDirection: 'row',
        width: '90%',
        height: 75,
        margin: 10,
        marginBottom: 5,
        alignSelf: 'center',
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    name: {
        fontSize: 20,
        marginTop: '5%',
        marginLeft: '10%',
        fontWeight: 'bold',
    },
    note: {
        fontSize: 18,
        color: '#882d52',
        marginLeft: '10%',
        fontWeight: 'bold',
        flexDirection: 'row',
    },
});

export default styles;
