import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ItemRankingBox = ({ users, navigation }) => {
    return (
        <View>
            {users.map((user) => {
                return (
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('UserPage', { user })}>
                            <View style={styles.card} key={users.id}>
                                <View>
                                    <Text style={styles.name}>{user.name}</Text>
                                    <View style={styles.note}>
                                        {user.skill.map((skill) => {
                                            return <Text style={styles.note}>{skill?.name}</Text>;
                                        })}
                                        <Text style={styles.note}>{user?.media}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                );
            })}
        </View>
    );
};

export default ItemRankingBox;
