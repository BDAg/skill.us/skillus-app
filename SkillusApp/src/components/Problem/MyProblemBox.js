import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';
import TagSkill from '../TagSkill/TagSkill';

//Componente de LISTAGEM DE PROBLEMAS DE UM USUÁRIO
const MyProblemBox = ({ problem }) => {
    return (
        <View>
            <View style={styles.card}>
                <View style={styles.row}>
                    <Text style={styles.skillName(problem.color)}>#{problem.id}</Text>
                    <Text numberOfLines={2} style={styles.title}>
                        {problem?.name}
                    </Text>
                </View>
                <TagSkill problem={problem} />
            </View>
        </View>
    );
};

export default MyProblemBox;
