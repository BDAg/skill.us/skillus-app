import React, { Component, useState } from 'react';
import { Text, View, Image } from 'react-native';
import styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ProblemDescription = ({ problem }) => {
    return (
        <View>
            <View>
                <Text style={styles.detailsText}>{problem?.description}</Text>
            </View>
        </View>
    );
};

export default ProblemDescription;
