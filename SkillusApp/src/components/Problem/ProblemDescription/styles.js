import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    detailsText: {
        backgroundColor: '#d5e1df',
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        marginTop: 10,
    },
});
