import React from 'react';
import { View, Text } from 'react-native';
import TagSkill from '../TagSkill/TagSkill';
import styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ProblemBox = ({ navigation, problem }) => {
    return (
        <View>
            <TouchableOpacity onPress={() => navigation.navigate('Problem', { problem })}>
                <View style={styles.card}>
                    <View style={styles.row}>
                        <Text style={styles.top}>#{problem?.id}</Text>
                        <Text numberOfLines={2} style={styles.title}>
                            {problem?.name}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <TagSkill problem={problem} />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default ProblemBox;
