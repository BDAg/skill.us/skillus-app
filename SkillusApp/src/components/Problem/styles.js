import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    card: {
        width: '90%',
        minHeight: 90,
        margin: 10,
        marginBottom: 5,
        paddingTop: 10,
        alignSelf: 'center',
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        fontSize: 17,
        marginLeft: 10,
        fontWeight: 'bold',
        maxWidth: '70%',
    },
    top: {
        fontSize: 18,
        color: '#ff6813',
        marginLeft: 10,
        fontWeight: 'bold',
    },
    row: {
        flexDirection: 'row',
        display: 'flex',
        flexWrap: 'wrap',
    },
    skillName(background) {
        return {
            color: background,
            fontSize: 18,
            marginLeft: 10,
            fontWeight: 'bold',
        };
    },
});

export default styles;
