import React, { useState } from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import TagSkill from '../../TagSkill/TagSkill';

const ProblemSkill = ({ problem }) => {
    return (
        <View style={{ flexDirection: 'row', flex: 1, flexWrap: 'wrap', width: '100%' }}>
            <TagSkill problem={problem} />
        </View>
    );
};

export default ProblemSkill;
