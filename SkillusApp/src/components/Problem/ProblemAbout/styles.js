import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    navTopper: {
        flexDirection: 'row',
        marginLeft: 10,
        alignSelf: 'flex-start',
        backgroundColor: '#F77F5C',
    },
});
