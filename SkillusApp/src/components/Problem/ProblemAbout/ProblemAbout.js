import React, { Component, useState } from 'react';
import { Text, View, Image } from 'react-native';
import styles from './styles';

const ProblemAbout = ({ problem }) => {
    const [expanded, setExpanded] = useState(false);

    function toggle() {
        setExpanded(!expanded);
    }

    function reduceDescription(description, maxLen = 117, separator = ' ') {
        if (description.length <= maxLen) return description;
        return description.substr(0, description.lastIndexOf(separator, maxLen)) + '...';
    }

    return (
        <View style={{ backgroundColor: '#f77f5c' }}>
            <View style={styles.navTopper}>
                <Text style={{ marginLeft: 15 }}># {problem?.id}</Text>
                <Text style={{ marginLeft: 15 }}>{problem?.name}</Text>
                <Text style={{ marginLeft: 15 }}>{problem?.createdBy}</Text>
            </View>
            <View>
                <Text>{problem?.status}</Text>
            </View>
        </View>
    );
};

export default ProblemAbout;
