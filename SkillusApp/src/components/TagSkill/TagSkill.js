import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const TagSkill = ({ problem }) => {
    return (
        <>
            {problem.skill.map((skill) => {
                return (
                    <View style={styles.tag}>
                        <Text style={styles.skill}>{skill?.name}</Text>
                    </View>
                );
            })}
        </>
    );
};

export default TagSkill;
