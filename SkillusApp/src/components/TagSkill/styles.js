import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    tag: {
        backgroundColor: '#ff6813',
        borderRadius: 12,
        height: 25,
        width: 85,
        alignItems: 'center',
        marginLeft: 10,
        marginBottom: 10,
    },
    skill: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold',
    },
});

export default styles;
