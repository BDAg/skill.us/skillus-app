import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    componentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Noto Sans',
        paddingTop: 15,
        paddingBottom: 30,
    },

    container: {
        // paddingTop: 25,
        paddingBottom: 15,
        width: 378,
        justifyContent: 'center',
        backgroundColor: "#fff",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        
        elevation: 4,
    },
    
    containerTitle: {
        textAlign: 'center',
        color: '#F77F5C',
        fontWeight: 'bold',
        fontSize: 24,
        display: 'flex',
        lineHeight: 33,
        alignItems: 'center',
        marginBottom: 10,
    },

    contactContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        // justifyContent: 'center',
    },

    contactInfo: {
        paddingLeft: 15,
        display: 'flex',
        fontSize: 20,
        lineHeight: 27,
        alignItems: 'center',
        paddingBottom: 15,
        color: '#AAAAAA'
    },
    
    contactIcon: {
        textAlign: "center",
        color: '#282828',
    },
    
    contactRow: {
        paddingLeft: 25,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },


})