import React, { Component } from 'react';
import { Text, View } from 'react-native';
import styles from './Styles';
import Icon from 'react-native-vector-icons/FontAwesome';

const ProfileContact = ({user}) => {

    return (
        <View style={styles.componentContainer}>
            <View style={styles.container}>
                <Text style={styles.containerTitle}> Informações de contato </Text>
                <View style={styles.contactContainer}>
                    {user.contact.map((contact) => (
                        <View style={styles.contactRow}>
                            <Icon name={contact.icon} backgroundColor="transparent" color="black" size={30} />

                            <Text style={styles.contactInfo}>{contact.name}</Text>
                        </View>
                    ))}
                </View>
            </View>
        </View>
    );
}

export default ProfileContact
