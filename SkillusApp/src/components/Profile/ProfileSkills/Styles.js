import { StyleSheet } from 'react-native';

function padding(a, b, c, d) {
    return {
        paddingTop: a,
        paddingRight: b ? b : a,
        paddingBottom: c ? c : a,
        paddingLeft: d ? d : b ? b : a,
    };
}

function calcTextColor(backgorund) {
    return parseInt(backgorund.replace('#', ''), 16) > 0xffffff / 2 ? '#282828' : '#f8f8f8';
}

export default StyleSheet.create({
    componentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Noto Sans',
        paddingTop: 15,
    },

    container: {
        // paddingTop: 25,
        paddingBottom: 15,
        width: 378,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },

    containerTitle: {
        color: '#F77F5C',
        fontWeight: 'bold',
        fontSize: 24,
        display: 'flex',
        lineHeight: 33,
        alignItems: 'center',
        marginBottom: 10,
    },

    skillsContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'center',
    },

    skillContainer(background) {
        return {
            backgroundColor: background,
            textAlign: 'center',
            borderRadius: 30,
            ...padding(0, 15, 0, 15),
            marginRight: 5,
            marginLeft: 5,
            marginBottom: 5,
        };
    },

    skillName(background) {
        return {
            color: calcTextColor(background),
            fontSize: 18,
            lineGeight: 27,
        };
    },
});
