import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import styles from './Styles';

const ProfileSkills = ({ user }) => {
    return (
        <View style={styles.componentContainer}>
            <View style={styles.container}>
                <Text style={styles.containerTitle}> Principais habilidades</Text>
                <View style={styles.skillsContainer}>
                    {user.skill.map((skill) => (
                        <View style={styles.skillContainer('#000000')}>
                            <Text style={styles.skillName('#000000')}>{skill.name}</Text>
                        </View>
                    ))}
                </View>
            </View>
        </View>
    );
};

export default ProfileSkills;
