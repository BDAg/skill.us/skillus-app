import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    componentContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Noto Sans'
    },
    
    container: {
        paddingTop: 25,
        paddingBottom: 15,
        width: 378,
        minHeight: 120,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#fff",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    
    spacer(options) {
        return {
            flex: 1, 
            justifyContent: options
        }
    },
    
    imgContainer: {
        marginTop: -80,
        width: 90,
        borderRadius: 360,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        
        elevation: 4,
    },
    
    profileImg: {
        height: 90,
        width: 90,
        borderRadius: 360,        
    },
    
    nameText: {
        fontSize: 22,
        color: '#882D52'
    },
    
    ratingText: {
        fontSize: 24,
        color: '#282828'
    },
    
    detailsText: {
        width: 331, 
        textAlign: 'center',
        fontSize: 20,
        color: '#AAAAAA'
    },
    
    moreText: {
        fontSize: 18,
        textAlign: 'center',
        color: '#F77F5C',
    },
    
});