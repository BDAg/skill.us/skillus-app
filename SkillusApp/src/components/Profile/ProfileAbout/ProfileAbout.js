import React, { Component, useState } from 'react';
import { Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './Styles';

const ProfileAbout = ({ user }) => {
    const [expanded, setExpanded] = useState(false);

    function toggle() {
        setExpanded(!expanded);
    }

    function reduceDescription(description, maxLen = 117, separator = ' ') {
        if (description.length <= maxLen) {
            return description;
        }
        return description.substr(0, description.lastIndexOf(separator, maxLen)) + '...';
    }

    return (
        <View style={styles.componentContainer}>
            <View style={styles.container}>
                <View style={styles.spacer('flex-start')} />
                <View style={styles.imgContainer}>
                    <Image style={styles.profileImg} source={require('../../../assets/danger.jpg')} />
                </View>
                <Text style={styles.nameText}>{user?.name}</Text>
                <View>
                    <Text style={styles.ratingText}>
                        <Icon name={'star'} backgroundColor="transparent" color="#FFD700" size={24} /> {user.media}
                    </Text>
                </View>

                <View style={styles.descriptionContainer}>
                    {!expanded ? (
                        <View>
                            <Text style={styles.detailsText}>{reduceDescription(user.description)}</Text>
                            <Text
                                style={styles.moreText}
                                onPress={() => {
                                    toggle();
                                }}>
                                Ver mais
                            </Text>
                        </View>
                    ) : (
                        <View>
                            <Text style={styles.detailsText}>{user?.description}</Text>
                            <Text
                                style={styles.moreText}
                                onPress={() => {
                                    toggle();
                                }}>
                                Ver menos
                            </Text>
                        </View>
                    )}
                </View>
                <View style={styles.spacer('flex-end')} />
            </View>
        </View>
    );
};

export default ProfileAbout;
