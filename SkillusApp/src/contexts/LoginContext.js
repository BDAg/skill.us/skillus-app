import React from 'react';

const LoginContext = React.createContext({
    token: undefined,
    user: undefined,
    updateToken: (token) => {},
    setUser: (user) => {},
});

export default LoginContext;
