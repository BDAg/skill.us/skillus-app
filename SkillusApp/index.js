import React, { Component } from 'react';
import App from './src/App';
import { name as appName } from './app.json';
import { ApolloProvider } from '@apollo/react-hooks';
import { setContext } from 'apollo-link-context';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { API } from 'react-native-dotenv';
import AsyncStorage from '@react-native-community/async-storage';
import { AppRegistry } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import LoginStack from './src/stack/LoginStack';
import LoginContext from './src/contexts/LoginContext';

const httpLink = createHttpLink({
    uri: API,
});

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = AsyncStorage.getItem('token');
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
        },
    };
});

const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
});

class Index extends Component {
    constructor(props) {
        super(props);

        this.updateToken = (token) => {
            if (token) {
                this.setToken(token);
            } else {
                this.removeToken();
            }
        };

        this.state = {
            token: false,
            updateToken: this.updateToken,
        };
    }

    async setToken(token) {
        await AsyncStorage.setItem('token', token);
        this.setState({ token });
    }

    async removeToken() {
        await AsyncStorage.removeItem('token');
        this.setState({ token: false });
    }

    render() {
        if (!this.state.token) {
            return (
                <ApolloProvider client={client}>
                    <LoginContext.Provider value={this.state}>
                        <NavigationContainer>
                            <LoginStack />
                        </NavigationContainer>
                    </LoginContext.Provider>
                </ApolloProvider>
            );
        }

        return (
            <ApolloProvider client={client}>
                <LoginContext.Provider value={this.state}>
                    <App />
                </LoginContext.Provider>
            </ApolloProvider>
        );
    }
}

AppRegistry.registerComponent(appName, () => Index);
